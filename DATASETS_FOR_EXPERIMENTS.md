# Datasets

## BestBuy
The same version as reference [25]
We used a custom script to scale this data to larger sizes.
The same data is also accesbile through the BestBuy API.

## IMDB
https://www.kaggle.com/ebiswas/imdb-review-dataset


## MSBuildings

We have the GeoJSON format (one file per state in the US). We can provide a link on an S3 bucket for the whole dataset if needed.
The data is also publicly availalble.

## Wikipedia
https://dumps.wikimedia.org/wikidatawiki/entities/

We downloaded the file latest-all.json.bz2
We downloaded on late September, as the file is updated regularly. Decompressed, the size is about 1.3TB.

## OpenStreetMap 21

The file that we used totals 2TB when decompressed, and it includes all geometry types (it combines all the files under OSM21 in the url).

We can provide S3 links for fast download if the whole data is needed.
